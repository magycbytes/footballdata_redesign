package com.magycbytes.footballdata;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */



public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private final String[] mPageTitles;
    private final Map<Integer, Fragment> mFragmentMap;

    public SectionsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        mPageTitles = new String[] {
                context.getString(R.string.leagues),
                context.getString(R.string.fixtures),
                context.getString(R.string.tables)
        };

        mFragmentMap = new HashMap<>(mPageTitles.length);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mFragmentMap.remove(position);
    }

    @Override
    public Fragment getItem(int position) {
        // TODO
//        Fragment fragment = FragmentsFactory.newInstance(position);
//        if (position == 0) {
//            LeaguesFragment leaguesFragment = (LeaguesFragment) fragment;
//            leaguesFragment.setActivityEvents(mActivityEvents);
//        }
//        mFragmentMap.put(position, fragment);
        return null;
    }

    @Override
    public int getCount() {
        return 0;
        // TODO
//        return mPageTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles[position];
    }
}