package com.magycbytes.footballdata;


import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alexandru on 6/12/16.
 */
public class ContentActivityView {

    @BindView(R.id.container)
    ViewPager mViewPager;
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public ContentActivityView(Activity activity) {
        ButterKnife.bind(this, activity.findViewById(R.id.main_content));
        setUpView(activity);
    }

    public void moveToFixtureTab() {
        mViewPager.setCurrentItem(1, true);
    }

    private void setUpView(Activity activity) {
        ((AppCompatActivity) activity).setSupportActionBar(mToolbar);
        FragmentManager fragmentManager = ((AppCompatActivity) activity).getSupportFragmentManager();

        mViewPager.setAdapter(new SectionsPagerAdapter(fragmentManager, activity));
        mTabLayout.setupWithViewPager(mViewPager);
    }
}
